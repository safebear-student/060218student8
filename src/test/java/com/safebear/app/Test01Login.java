package com.safebear.app;

import com.safebear.app.pages.LoginPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 06/02/2018.
 */
public class Test01Login extends BaseTest{
    @Test
    public void testLogin(){
        //step 1 confirm we're on the login page
        assertTrue(welcomePage.checkCorrectPage());
        //step 2 click on the login link and the login page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
        //step 3 login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }
}
